# LinDoA

This project contains code related to the project LinDoA.

The algorithms have two purposes, estimating direction of arrival (DoA) and reconstructing the signals (Beamforming).
All methods work for single sources, and some also work for multiple sources. Pre-recorded sound data can be downloaded from e.g. Shtooka.net.

The following algorithms are implemented
- LinDoA Beamformer and Doa Estimator
- Differential LinDoA (Diff-LinDoA) Beamformer and Doa Estimator
- Time-constrained LinDoA (TC-LinDoA) Beamformer and Doa Estimator
- Delay and Sum (DaS) Beamformer and DoA estimator
- Minimum Variance Distortionless Response (MVDR) Beamformer and Doa Estimator
- Capon DoA Estimator
- Multi-Channel Cross-Correlation (MCCC) DoA Estimator 
- Normalized Cross-Correlation (NCC) DoA Estimator 
- Multiple Signal Classification (MUSIC) DoA Estimator 
