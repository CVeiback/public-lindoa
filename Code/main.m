%% Evaluation of Methods for Direction of Arrival Estimation
% Author: Clas Veib�ck
% Description: Simulates sound from a source in free-field and estimates
% the direction of arrival. The main purpose of the script is to
% demonstrate the usage of different methods.

%% Clean
clear all
close all
addpath('common');

%% Sound Data
% For this simulation various datasets have been used.
% In particular the datasets from Shtooka.net:
% - eng-wims-mary 
% - eng-balm-judith
dataset_path = 'c:\Data\Sound\Phrases\';
%dataset_path = 'Phrases\';

%% Scenario Parameters
c = 343;    % Speed of sound

% Signal
time = 3;               % Seconds
scenario = 'mary';      % Signal scenarios (e.g. 'wgn', 'mary', 'judith')

% Source directions of arrival
doa = [8*pi/9 pi/4]';

% Geometry of array
M = 8;              % Number of microphones
r = 0.1;            % Radius of circular array
noise = 0.0125;     % Noise standard deviation

%% Estimation Parameters
sampling_factor = 3;    % Downsampling factor

%% Generate/Load Signals
% White gaussian noise for all sources
if strcmpi(scenario, 'wgn')
  fs = 8000;
  samples = fs * time;
  ys = randn(samples,length(doa));
end

% Woman voice speaking phrases
if any(strcmpi(scenario, {'mary', 'judith'}))
    
  folder = [dataset_path scenario '\'];
  ext = '.flac';
  files = dir([folder '*' ext]);
  files([1 2]) = []; % Remove unsuitable files
  
  ai = audioinfo(fullfile([files(1).folder], [files(1).name]));
  num_samples = ai.SampleRate * time;
  yr = zeros(num_samples, length(doa));
  for i = 1:length(doa)
      yt = audioread(fullfile([files(i).folder], [files(i).name]));
      samples = min(num_samples, size(yt,1));
      yr(1:samples,i) = yt(1:samples,1);
  end
  y = downsample(yr, sampling_factor);
  fs = ai.SampleRate / sampling_factor;
  Y = normalize(y);
end

SNR = 10*log10(var(Y)/noise^2);

%% Define Sensor Array
% Array frame
% pos = [-84 -84 -145/2 -145/6 145/6 145/2  84   84; ...
%        -87 -39    9      9     9     9   -39  -87; ...
%          0   0    0      0     0     0     0    0]/1000; % m
     
% Circular array
angles = 2*pi * (0:M - 1)/M;
pos = [cos(angles); sin(angles);zeros(1,M)] * r;

%% Simulate Response
sound = simulate(Y, fs, pos, doa, noise);
N = size(sound,1);

%% Pre-filter of Response
fLow = 2000;
[B, A] = butter(2,fLow/(fs/2));
lowSound = filtfilt(B,A,sound);

%% Initialize DoA Estimation
% Grid DoA in a number of points in a region of interest
NDoA = 200;
Thl = linspace(0,pi,NDoA);

% Size of each segment to be processed individually
segmentLength = round(fs/4);

% Overlap between segments
stepLength = round(segmentLength/6);

% Determine total number of segments for pre-allocation of memory
I = [1 N]; % Determine portion of sound to use
Ntot = range(I) + 1;
Nsteps = floor(Ntot / stepLength);

% Pre-allocate variables if necessary
Ps = zeros(Nsteps, NDoA);
Pl = zeros(Nsteps, NDoA);
rhol = zeros(Nsteps, NDoA);
xcl = zeros(Nsteps, NDoA);
Plm = zeros(Nsteps, NDoA);
xclm = zeros(Nsteps, NDoA);
kurt = zeros(Nsteps, NDoA);
Pld = zeros(Nsteps, NDoA);
Pc = zeros(Nsteps, NDoA);
Pm = zeros(Nsteps, NDoA);
lags = zeros(Nsteps, size(sound,2));
quality = zeros( 2, Nsteps);
Pmva = zeros(Nsteps, NDoA);
Pmu = zeros(Nsteps, NDoA);
Pmv = zeros(Nsteps, NDoA);
Ptrue = zeros(Nsteps, length(doa));

%% DoA Estimation
for i = 1:Nsteps
    if mod(i,1) == 0; clc; disp( [num2str(round(100*i/Nsteps,1)) '%']); end
    
    index = I(1) + stepLength*(i-1) + (1:segmentLength);
    index(index > I(end)) = [];    
    Ptrue(i,:) =  sqrt(sum(Y(index,:).^2));   
    
    Z = lowSound(index,:);
    
    [~, Ps(i,:)] = delayAndSum(Z,Thl,pos,fs, 'spectral');
    
    [~, Pl(i,:), rhol(i,:), xcl(i,:)] = linDoA(Z,Thl,pos,1);     
    [~, Plm(i,:), kurt(i,:), xclm(i,:)] = tcLinDoA(Z,Thl,pos,1,fs);
    [~, Pld(i,:)] = diffLinDoA(Z,Thl,pos,1);

    Z = sound(index,:);
    [~, Pc(i,:), lags(i,:), quality(:,i)] = ncc(Z,Thl,pos,fs);
    [~, Pm(i,:)] = mccc(Z,Thl,pos,fs);
    [~, Pmva(i,:)] = mvdr(Z,Thl, pos, fs);
    
    for f = 100:100:3000
      Pmu(i,:) =  Pmu(i,:) + music(Z, Thl, pos, f, 4);
      Pmv(i,:) = Pmv(i,:) + capon(Z, Thl, pos, f);
    end
end
return
%%
Ptrue = zeros(Nsteps, length(doa));
for i = 1:Nsteps
    if mod(i,1) == 0; clc; disp( [num2str(round(100*i/Nsteps,1)) '%']); end
    
    index = I(1) + stepLength*(i-1) + (1:segmentLength);
    index(index > I(end)) = [];
    Ptrue(i,:) =  sqrt(sum(Y(index,:).^2));    
end
%%
N = size(Pl,1);
ang = zeros(8, N);

ang(1,:) = showResults(Thl,[0 time], Pl','LinDoA',doa);
ang(2,:) = showResults(Thl,[0 time], Plm','TCLinDoA',doa);
ang(3,:) = showResults(Thl,[0 time], Pm','MCCC',doa);
ang(4,:) = showResults(Thl,[0 time], -Pc','NCC',doa);
ang(5,:) = showResults(Thl,[0 time], Ps','DaS',doa);
ang(6,:) = showResults(Thl,[0 time], Pmu','MUSIC',doa);
ang(7,:) = showResults(Thl,[0 time], Pmv','Capon',doa);
ang(8,:) = showResults(Thl,[0 time], Pmva','MVDR',doa);
% TODO: Compare various transformations of the matrices for improved DoA
% estimation. E.g. normalize(Pl)' seems to improve the results.

figure; 
estTimes = (I(1) + segmentLength/2 + stepLength * (0:Nsteps-1) )/fs;
plot(estTimes([1 end]),rad2deg([doa'; doa']), '--', 'Color', [0.3 0.3 0.3],'HandleVisibility', 'off','LineWidth',2)
hold on;
set(gca,'ColorOrderIndex',1);
plot(linspace(0,10,N), rad2deg(ang),'.','MarkerSize', 12)
ylabel('Direction of Arrival (deg)');
xlabel('Time (s)');
legend('LinDoA', 'TCLinDoA','MCCC', 'NCC', 'DaS', 'MUSIC', 'Capon', 'MVDR', 'Location', 'bestoutside');

%% Estimate signal using true DoA

% Delay and Sum methods
tic; [S0s] = delayAndSum(sound,doa,pos,fs, 'spectral'); toc

% LinDoA methods
tic; [S0lm] = tcLinDoA(sound,doa',pos,1,fs);toc
tic; [S0l] = linDoA(sound,doa',pos,1);toc
tic; [S0lmc] = tcLinDoA(sound,doa,pos,1,fs);toc
tic; [S0lc] = linDoA(sound,doa,pos,1);toc
tic; [S0ld] = diffLinDoA(sound,doa,pos,1);toc

%% Estimate signal using estimated DoA
if ~exist('doa_hat','var')
    doa_hat = doa;
end

% Delay and Sum methods
tic; [Ss] = delayAndSum(sound,doa_hat,pos,fs, 'spectral'); toc

% LinDoA methods
tic; [Slm] = tcLinDoA(sound,doa_hat',pos,1,fs);toc
tic; [Sl] = linDoA(sound,doa_hat',pos,1);toc
tic; [Slmc] = tcLinDoA(sound,doa_hat,pos,1,fs);toc
tic; [Slc] = linDoA(sound,doa_hat,pos,1);toc
tic; [Sld] = diffLinDoA(sound,doa_hat,pos,1);toc

%% Reconstruct Signals

%% Correlation between reconstructed signals and each source
Nsigs = length(doa);
Nestimators = 3;
corr0 = zeros(Nsigs,Nestimators);
lags0 = zeros(Nsigs,Nestimators);
for i =1:Nsigs  
  X = normalize([S0s(:,i) S0lm(:,2*i) S0l(:,2*i)]);  

  for j = 1:Nestimators
    [xc,lags]= xcorr(X(:,j),normalize(Y(:,i)));
    [corr0(i,j), ind] = max(xc);
    lags0(i,j) = lags(ind);
  end
end

%% Original correlation between recording and each source
corrm = zeros(length(doa),size(sound,2));
for i = 1:length(doa)
  for j = 1:size(sound,2)
    [corr,lags] = xcorr(sound(:,j),Y(:,i));
    corrm(i,j) = max(corr);
  end
end

%% Correlation between integrated signal derivative and each source
Sc = S0l(:,[2 4]);
corr0 = zeros(length(doa),size(Sc,2));
for i = 1:length(doa)
  for j = 1:size(Sc,2)
    corr = xcorr(normalize(Sc(:,j)),normalize((Y(:,i))));
    corr0(i,j) = max(corr)/size(Y,1);
  end
end

%% Correlation between integrated signal derivative and each source
Sc =  Sld(:,[2 3]);
corre = zeros(length(doa),size(Sc,2));
for i = 1:length(doa)
  for j = 1:size(Sc,2)
    corr = xcorr(normalize(cumsum(Sc(:,j))),normalize((Y(:,i))));
    corre(i,j) = max(corr)/size(Y,1);
  end
end

%% Presentation
[B, A] = butter(2, [20 2000] / (fs/2)); 
lp = @(x) (filtfilt(B,A,x));

for i = 1:length(doa)
    disp('Original'); soundsc(Y(:,i),fs);pause(length(Y)/fs);
    disp('Recording'); soundsc(sound(:,1),fs);pause(1+length(sound)/fs);
    
    disp (['True DoA for Signal ' num2str(i)])
    disp('DaS');soundsc(S0s(:,i),fs);pause(0.5+length(S0s)/fs);
    disp('MLinDoa Signal');soundsc(S0lm(:,2*i-1),fs);pause(0.5+length(S0lm)/fs);
    disp('LinDoa Signal');soundsc(S0l(:,2*i-1),fs);pause(0.5+length(S0l)/fs);
    disp('MLinDoa Derivative');soundsc(S0lm(:,2*i),fs);pause(0.5+length(S0lm)/fs);
    disp('LinDoa Derivative');soundsc(S0l(:,2*i),fs);pause(0.5+length(S0l)/fs);
    disp('MLinDoa Integrated');soundsc(intFFT(S0lm(:,2*i)),fs);pause(0.5+length(S0lm)/fs);
    disp('LinDoa Integrated');soundsc(intFFT(S0l(:,2*i)),fs);pause(0.5+length(S0l)/fs);
    disp('DiffLinDoa Derivative');soundsc(S0ld(:,1+i),fs);pause(0.5+length(S0l)/fs);
    disp('DiffLinDoa Integrated');soundsc(intFFT(S0ld(:,1+i)),fs);pause(0.5+length(S0l)/fs);
    
    
    disp (['Estimated DoA for Signal ' num2str(i)])
    disp('DaS');soundsc(Ss(:,i),fs);pause(0.5+length(Ss)/fs);
    disp('LinDoa Separately');soundsc(Sl(:,2*i-1),fs);pause(0.5+length(Sl)/fs);
    disp('MLinDoa Separately');soundsc(Slm(:,2*i-1),fs);pause(0.5+length(Sl)/fs);
    disp('MLinDoa Combined');soundsc(Slmc(:,2*i-1),fs);pause(0.5+length(Sl)/fs);
    disp('LinDoa Derivative Separately');soundsc(Sl(:,2*i),fs);pause(0.5+length(Sl)/fs);
    disp('MLinDoa Derivative Separately');soundsc(Slm(:,2*i),fs);pause(0.5+length(Sl)/fs);
    disp('LinDoa Derivative Together (singular)');soundsc(Slc(:,2*i),fs);pause(0.5+length(Slc)/fs);
    disp('LinDoa Derivative Combined');soundsc(Sld(:,1+i),fs);pause(0.5+length(Sld)/fs);
end