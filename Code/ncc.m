function [S, P, lags, quality] = ncc(Y, doa, pos, fs)
%NCC Broadband far-field beamformer (NCC)
% Performs beamforming and DoA estimation using the NCC method, which is 
% broadband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   fs      Sampling frequency of signal
%
% Author    Clas Veib�ck
  
  n = size(Y, 2);
  N = size(Y, 1);
  sos = 343;
  maxlag = 50; % Compute from position array
  
  %% Correlations
    
  % Expected time delay for the sensors given an angle to the source
  tau = @(angle)([cos(angle);sin(angle);zeros(size(angle))]' * pos / sos);
  
  % Find expected sample delays of sensors. Subtraction of mean gives 
  % absolute reference for comparison.
  taua = tau(doa)*fs;
  tau0 = taua - mean(taua,2);

  [corr, originalLags] = correlationMatrix(Y,maxlag);
   
  % Do the best of the correlations
  % We can project this estimated taus to a valid delay matrix. However, we
  % are only interested in the mean
  %correctedLags = sum(originalLags)/(n-1) + sum(originalLags,2)/(n-1);
  lags = sum(originalLags)/(n-1);
  
  % Compute correlation matrix at corrected lags to determine quality
  % (expensive)
  %corrn = correlationMatrix(Y,maxlag,lagsn);
  
  % Measurement of amount of correlation in the signals
  sigma = svd(corr); quality(1) = sigma(1)/n;
  quality(2) = (sum(corr(:))-sum(diag(corr)))/(n^2-n);
  %sigma = svd(corrn); quality(3) = sigma(1)/n;
  %quality(4) = (sum(corrn(:))-sum(diag(corrn)))/(n^2-n);
  
  tauDiff = tau0 - lags;
  oneNorm = sum(abs(tauDiff),2); % Should be less sensitive to outliers
  %twoNorm = sqrt(sum(tauDiff.^2,2))';
  
  %% Return results
  P = oneNorm;
  S = delayAndSumSignal(Y, lags); % Delay and sum using estimated corr!
end