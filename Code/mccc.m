function [R, P] = mccc(Y, doa, pos, fs)
%MCCC Broadband far-field doa (MCCC)
% Performs DoA estimation using the multi-channel cross-correlation method, 
% which is broadband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   cost    Cost function to employ: 'MCCC'(default), 'SLPM', 'BMUSIC'
%
% Author    Clas Veib�ck

  [N, n] = size(Y);
  c = 343;
      
  F = cell(n,1);
  for j = 1:n
    F{j} = griddedInterpolant(Y(:, j), 'spline');
  end
  
  R = zeros(n,n,length(doa));
  P = zeros(1, length(doa));
  
  v = [cos(doa); sin(doa);zeros(size(doa))];
  dt = -pos'*v/c;
  ds = dt*fs;
  ds = ds - min(ds);  % Relative sample offset  
  
  St = zeros(N,numel(doa),n);
  for j = 1:n
      St(:,:,j) = F{j}(ds(j,:)+(1:N)');
  end
  for i =1:numel(doa)
      Ss = squeeze(St(:,i,:));
      R(:,:,i) = Ss'*Ss;
      P(i) = -logDet(R(:,:,i), 'chol');
  end

end
