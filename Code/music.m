function P = music(Y, doa, pos, f, nsignal)
%MUSIC Narrowband far-field MUSIC beamformer
% Performs DoA estimation using the MUSIC method, which is 
% narrowband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   f       Frequency of signal
%   nsignal Number of sources (optional)
%
% Author    Clas Veib�ck

  n = size(Y, 2);
  c = 343;
  w = 2*pi*f;

  P = nan(size(doa));
  R = 1/n*(Y'*Y);
  [U, S] = svd(R);
  if nargin < 5
    nsignal = sum(diag(S)> 0.01*S(1));
  end
  Uhat = U(:, nsignal+1:end);
  for i=1:numel(doa)
    v = [cos(doa(i)); sin(doa(i));0];
    d = -pos'*v;
    a = exp(1i*d/c*w);
    P(i) = real((a'*a)/(a'*(Uhat*Uhat')*a + eps(1))); % Real since R is Hermitian
  end
end
