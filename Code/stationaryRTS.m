function [ xn, Pn ] = stationaryRTS(Y, H, R, F, Q)
%STATIONARYRTS Stationary Rauch-Tung-Striebel smoother
% Computes the smoothing estimate using a stationary Rauch-Tung-Striebel
% smoother. The covariances and gains are assumed stationary and are 
% pre-computed for improved computational performance.
%   Y       Signal recordings
%   H       Observation matrix
%   R       Observation noise covariance
%   F       Transition matrix
%   Q       Process noise covariance
%
% Author    Clas Veib�ck

[m, N] = size (Y);
[m1, n] = size(H);
[n1, n2] = size(F);
[m2, m3] = size(R);
[n3, n4] = size(Q);

tol = 0.01;

if any([n1 n2 n3 n4] ~= n) || any([m1 m2 m3] ~= m)
    error('Dimension mismatch');
end

Ppf = 1e9*eye(size(F));
erro = inf;
for i = 1:20    
    S = H * Ppf * H' + R;
    Kf = Ppf * H' / S;
    Pff = Ppf - Kf * S * Kf'; 
    
    Ppn = F * Pff * F' + Q;
    errn = sum((Ppn(:)-Ppf(:)).^2);
    Ppf = Ppn;
    if errn >= (1-tol)*erro; break; end
    erro = errn;
end
Bf = eye(size(F))-Kf*H;
A = Bf * F;
KY = Kf*Y;
Ks = Pff * F' / Ppf;
Pn = Pff;
erro = inf;
for i = 1:20
   Po = Pn;
   Pn = Pff + Ks * (Pn - Ppf) * Ks';
   errn = sum((Pn(:)-Po(:)).^2);
   if errn >= (1-tol)*erro; break; end
   erro = errn;
end

x = ltitr(A,eye(2),[KY(:,2:end) [0;0]]',KY(:,1))';

xf = [fliplr(x(:,1:end-1)) zeros(n,1)];
u = xf - (Ks * F) * xf;
xn = fliplr(ltitr(Ks,eye(2),u',x(:,N))');

% Actual implementation
% x = zeros(n,N); % x_{k|k}
% xp = zeros(n,N); % x_{k|k-1}
% xn = x;
% x(:,1) = KY(:,1);
% for k = 2:N
%    x(:,k) = A * x(:,k-1) + KY(:,k);
% end
% xp = F * x; 
% xn(:,N) = x(:,N);
% for k = N:-1:2       
%    xn(:,k-1) = x(:,k-1) + Ks * (xn(:,k) - xp(:,k-1));
% end

end

