function [S, P] = mvdr(Y, doa, pos, fs)
%MVDR Broadband far-field MVDR DoA estimator and beamformer
% Performs DoA estimation and beamforming using the MVDR method, which is 
% broadband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   f       Frequency of signal
%
% Author    Clas Veib�ck

  [N, n] = size(Y);
  c = 343;
  
  F = cell(n,1);
  for j = 1:n
    F{j} = griddedInterpolant(Y(:, j), 'spline');
  end
  
  P = nan(size(doa));
  S = zeros(N,numel(doa));
  for i=1:numel(doa)
      
    Ya = zeros(N,n);
    v = [cos(doa(i)); sin(doa(i));zeros(size(doa(i)))];
    dt = -pos'*v/c;
    ds = dt*fs;  ds = ds - min(ds);  % Relative sample offset
    
    for j=1:n
      Ya(:,j) = F{j}(ds(j)+(1:N)');
    end
    Ra = Ya'*Ya / n;          
  
    a = ones(n,1); % This is the relative gain of the microphones
    w = Ra \ a;
    P(i) = real(1/(a' * w)); % Real since R is Hermitian
    w = w * P(i);
    S(:,i) = Ya*w;
  end
end