function sx = intFFT(x, fs)
%INTFFT Integrate a vector in the frequency domain
% Integrates a vector sampled at the rate fs in the frequency domain.
% Columnwise if signal is matrix.
%   x       Signal vector
%   fs      Sampling frequency (fs = 1)
%
% Author    Clas Veib�ck

if nargin < 2
    fs = 1;
end

N = size(x,1);

iiw = N ./ (2 * pi * 1i * fs * rem([0:(N-1)/2 ceil(-N/2):-1]',N/2));

iiw(1) = 0;
if mod(N,2) == 0
  iiw(N/2+1)=1; % Required to be able to recover signal
  %iiw(N/2+1)=0; % Definition of integral(?)
end

X = fft(x);
dX = X .* iiw;
sx = (ifft(dX));
end

