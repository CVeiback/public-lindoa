function angles = showResults(angle, time, P, name, doa)
%SHOWRESULTS Displays the results for the segments in time and the grid
%points.
% An image is shown with the maximum angle for each segment overlayed, as
% well as a moving median.
%   angle   Angles in grid (radians)
%   time    Start and end time (uniform sampling assumed in between)
%   P       Result to be display (high value -> better match)
%   name    Name to display on plot
%   doa     True angles (optional)
%
% Author    Clas Veib�ck

if nargin < 5; doa = []; end

figure;
imagesc(time([1 end]),rad2deg(angle), normalize(P)); 
set(gca,'YDir','normal')
ylabel('Direction of Arrival (deg)');
xlabel('Time (s)');
hold on;

N = size(P,2);
t = time(1) + (0:N-1)/N*(time(end)-time(1));

[~,ind] = max(P); 
angles = angle(ind);
plot(t, rad2deg(movmedian(angles,5)), 'LineWidth', 2);

if ~isempty(doa)
  plot(t([1 end]),rad2deg([doa'; doa']), '--', 'Color', [0.3 0.3 0.3],'HandleVisibility', 'off','LineWidth',2)
end

title(name)
end

