function S = delayAndSumSignal(Y, ds)
%DELAYANDSUMSIGNAL Delay and sum signal in continuous time. Units are
%samples, which can be fractional.
% Gridded interpolant with spline is used.
%   Y       Signals to delay and sum
%   ds      Number of samples to delay for each signal before summing
%
% Author    Clas Veib�ck

N= size(Y,1);
n = size(ds,1);
S = zeros(N,n);

for j = 1:size(Y,2)
    F = griddedInterpolant(Y(:, j), 'spline');
    for i = 1:n
        S(:,i) = S(:,i) + F((1:N)' - ds(i,j));
    end
end
end

