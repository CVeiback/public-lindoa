function [aligned, prepad, postpad] = alignSignal(signal,T, trim)
%ALIGNSIGNAL Align signals in discrete time units.
% Shifts the signals to align according to a given offset for each signal
%   signal  The signals to align
%   T       Sample offset for each signal
%   trim    Trim the end points to retain signal length (default false)
%
% Author    Clas Veib�ck

N = size(signal,1);
n = length(T);
if size(T,1) > 1; T = T'; end

if nargin < 3; trim = false; end

if size(signal,2) == 1
    signal = repmat(signal,1,n);
end

prepad = -min(T);
postpad = max(T);

if trim
    ind = (1:N)' + T - min(T);
    aligned = zeros(size(signal));

    signal = [zeros(prepad,n);signal;zeros(postpad,n)];
    for i = 1:n
        aligned(:,i) = signal(ind(:,i),i);
    end
else
    ind = (1:N)' + T - min(T);
    aligned = zeros(prepad + N + postpad, n);
    for i = 1:n
        aligned(ind(:,i),i) = signal(:,i);
    end
end
end

