function [corr, lags] = correlationMatrix(Y, maxlag, atLags)
%CORRELATIONMATRIX Find best correlation between all combinations of
%columns
%   Y       Signals
%   maxlag  Maximum number of lags to consider
%   atLags  Return correlation matrix for given lags(leave empty otherwise)
%
% Author    Clas Veib�ck

  [N, n] = size(Y);
  
  if nargin < 2 || isempty(maxlag)
      maxlag = N-1;
  end
  lagsGiven = ~(nargin < 3 || isempty(nargin));
  
  lags = zeros(n,n);
  corr = zeros(n,n);
  for i = 1:n
    for j = i+1:n
        % TODO: Pre-compute FFT of the signals and reuse in own
        % implementation. Allow zero-padding to improve resolution.
        % Use Martin's implementation which is equal to built-in.
        [c, l]= xcorr(Y(:,i),Y(:,j), maxlag, 'normalize');
        
        if lagsGiven 
            ind = (round(atLags(j) - atLags(i))==l);
            corr(i,j) = c(ind);
        else
            [corr(i,j), ind] = max(c);
        end
        lags(i,j) = l(ind);   
    end
  end
  lags = lags - lags';
  corr = corr + corr' + eye(n);  
end

