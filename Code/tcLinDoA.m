function [S, MSE, kurtosis, xc] = tcLinDoA(Y,doa,pos, ndeg, fs, directTime)
%TCLINDOA Broadband far-field TCLinDoA beamformer and DoA estimator
% Performs beamforming and DoA estimation using the time-constrained LinDoA 
% method, which is broadband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   ndeg    Degree of Taylor polynomial
%   fs      Sampling rate
%   directTime  Flags that doa contains time delays rather than directions
%
% Author    Clas Veib�ck

    c = 343;    
    
    % SLS
    [n, N] = size(doa);
    [M, m] = size(Y);
    
    if nargin < 6; directTime = false; end
    if nargin < 5; fs = 48000; end
    if nargout > 1; SE = true; else; SE = false; end
    if nargout > 2; Gaussianity = true; else; Gaussianity = false; end
    if nargout > 3; corr = true; else; corr = false; end
  
    if directTime
        N = 1;
        n = size(doa,2);
    end
    nl = 0:ndeg;
    Tinit = repmat(1./factorial(nl), [1 n]);

    MSE = zeros(1,N);
    xc = zeros(1,N);
    kurtosis = zeros(n,N);
    S = zeros(M,n*(ndeg+1),N);
    for i = 1:N
        if directTime
            dt = doa;
        else
            v = [cos(doa(:,i)'); sin(doa(:,i)'); zeros(size(doa(:,i)'))];
            dt = -pos'*v/c;
        end
        T = Tinit.*kron(-dt,ones(1,ndeg+1)).^kron(ones(1,n),nl);

        H = T;
        R = eye(m);
        F = kron( eye(n),[1 1/fs; 0 1]);
        Q = kron(eye(n),[0 0; 0 1]); % Assumes second order
        [St, ~] = stationaryRTS(Y', H, R, F, Q);
        S(:,:,i) = St';
        
        if SE
          Yhat = H * St;
          eps = Y - Yhat';
          MSE(i) = -sum(eps(:).^2);
        end
        if Gaussianity
            Sm = St(1:2:end,:); 
            Sm = Sm - mean(Sm,2);
            Cm  = mean(Sm.^2,2).^2;
            kurtosis(:,i) = (mean(Sm.^4,2) ./ Cm - 3).^2;
        end
        if corr
            Sw = normalize(St');
            Rs = (Sw.'*Sw)/size(Sw,1);
            xc(i) = logDet(Rs);
        end
    end
  end