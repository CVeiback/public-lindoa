%% Simulate sound impinging on an array from a given direction
% Author: Clas Veib�ck
% Description: Simulates sound from a source in free-field, by
% interpolating the sound.
% y     The sound data, one for each source
% fs    Sampling rate
% pos   Positions of microphones
% angle Directions of arrival
% stdev Standard deviation of additive noise

function [Y, fs, pos] = simulate(y, fs, pos, angle, stdev)

if nargin < 5
    stdev = 0;
end

[N, num_signals] = size(y);
num_sensors = size(pos,2);

c = 343;

t = (0:N-1);
max_dist = max(sqrt(sum(pos.^2,1)));
trim = ceil(max_dist / c * fs);
len = N - 2 * trim;

Y = stdev * randn(len, num_sensors); % Pre-add noise
for j = 1:num_signals
    doa = [cos(angle(j)); sin(angle(j));0];
    tau = -pos' * doa / c;
    delay = tau * fs;
    
    F = griddedInterpolant(t, y(:,j), 'spline');
    for i = 1:num_sensors
        ts = (trim - delay(i) + (0:len-1))';
        Y(:,i) = Y(:,i) + F(ts);
    end
end


