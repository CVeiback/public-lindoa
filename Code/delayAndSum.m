function [S, P] = delayAndSum(Y, doa, pos, fs, domain, dir)
%DELAYANDSUM Broadband far-field Delay and Sum beamformer and Doa
%estimator
% Performs beamforming and DoA estimation using the Delay and sum method, 
% which is broadband, data-independent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   fs      Sampling frequency of signal
%   domain  Domain of computation: 'temporal', 'spectral' (default)
%
% Author    Clas Veib�ck

  n = size(Y, 2);
  N = size(Y, 1);
  c = 343;
  
  Y = normalize(Y);
  
  if nargin < 6 || isempty(dir); dir =[];  end
  if nargin < 5 || isempty(domain); domain = 'spectral'; end
  
  if strcmpi(domain, 'spectral')
    %w = 2 * pi * [0:N/2  (-N/2+1):-1]' / N;
    w = 2 * pi * [0:floor((N-1)/2) (-floor(N/2):-1)]' / N;

    Yf = fft(Y);
    R = zeros(N,length(doa));
  else
    F = cell(n,1);
    for j = 1:n
      F{j} = griddedInterpolant(Y(:, j), 'spline');
    end
  end
  
  %ind = true(1,n);
  sw = ones(1,n)/n;
  
  S = zeros(N,length(doa));
  for i=1:numel(doa)
    v = [cos(doa(i)); sin(doa(i)); zeros(size(pos,1)-2,1)];
    if ~isempty(dir) 
        %ind = (v'* dir) >= -0.1;
        sw = (v'* dir); sw(sw < 0) = 0; sw = sw/sum(sw);
    end
    %dt = pos(:,ind)'*v/c;
    dt = pos'*v/c;
    ds = dt*fs;  ds = ds - min(ds);  % Relative sample offset
    
    if strcmpi(domain, 'spectral')
      %R(:,i) = mean(Yf(:,ind) .* exp(-1i*w*ds'),2); %Subtraction of delay to estimate 
      R(:,i) = sum((sw .* Yf) .* exp(-1i*w*ds'),2); %Subtraction of delay to estimate 
    else
      %indf = find(ind);
      for j=1:n
          S(:,i) = S(:,i) + F{j}((1:N)'-ds(j)).*sw(j); %Subtraction of delay to estimate
      end
    end
  end
  
  if strcmpi(domain, 'spectral')
    S = ifft(R);
  end
  P = real(sum(S.*S,1));
  S = real(S);% / n;
end
