function P = capon(Y, doa, pos, f)
%CAPON Narrowband far-field Capon DoA estimator
% Performs DoA estimation using the Capon method, which is 
% narrowband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   f       Frequency of signal
%
% Author    Clas Veib�ck

  n = size(Y,2);
  c = 343;
  w = 2*pi*f;

  P = nan(size(doa));
  R = 1/n*(Y'*Y);
  for i=1:numel(doa)
    v = [cos(doa(i)); sin(doa(i));0];
    d = -pos'*v;
    a = exp(1i*d/c*w);
    P(i) = real(1/(a' * (R \ a))); % Real since R is Hermitian
  end
end
