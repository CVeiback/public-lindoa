function [S, MSE, rho2, xc,epss] = linDoA(Y, doa, pos, ndeg)
%LINDOA Broadband far-field LinDoA beamformer and DoA estimator
% Performs beamforming and DoA estimation using the LinDoA method, 
% which is broadband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   ndeg    Degree of Taylor polynomial
%
% Author    Clas Veib�ck

  c = 343;
  
  [n, N] = size(doa);
  [M, m] = size(Y);
    
  if nargout > 1; SE = true; else; SE = false; end
  if nargout > 2
    Pearson = true; 
    Yn = normalize(Y);
  else
    Pearson = false; 
  end
  if nargout > 3; corr = true; else; corr = false; end
  if nargout >= 5
    epss = zeros(size(Y,1),size(doa,2));
  end
  
  nl = 0:ndeg;
  Tinit = repmat(1./factorial(nl), [1, n]);
  MSE = nan(size(doa));
  rho2 = nan(size(doa));
  xc = nan(size(doa));
  if n~=m
    S = zeros(M,(ndeg+1)*n,N);
  else
    S = zeros(M,(ndeg+1)*1,N);
  end
  for i=1:size(doa,2)
    
    % Full signals
    if n == m
      dt = doa(:,i);
    else
      v = [cos(doa(:,i)'); sin(doa(:,i)');zeros(size(doa(:,i)'))];
      dt = -pos'*v/c;
    end
    T = Tinit.*kron(-dt,ones(1,ndeg+1)).^kron(ones(1,n),nl);
    
    Shat = T\Y';
    
    if SE
      Yhat = T*Shat;
      eps = Y-Yhat';
      MSE(i) = -sum(eps(:).^2);
      if nargout >= 5
        epss(:,i) = sum(eps,2);
      end
    end
    if Pearson
      tau = round(dt*48000);
      Sm = alignSignal(Shat(1,:)',tau, true);
      epsp = Yn.*normalize(Sm);
      rho2(i) = mean(epsp(:));
    end
    if corr
      Sw = normalize(Shat');
      Rs = (Sw.'*Sw)/size(Sw,1);
      xc(i) = logDet(Rs);
    end
    
    S(:,:,i) = Shat';
  end
end
