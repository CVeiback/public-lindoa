function [S, P] = diffLinDoA(Y, doa, pos, ndeg)
%DIFFLINDOA Broadband far-field diffLinDoA beamformer and DoA estimator
% Performs beamforming and DoA estimation using the diffLinDoA method, 
% which is broadband, data-dependent and far-field.
%   Y       Signal recordings
%   doa     Directions of Arrival
%   pos     Positions of sensors
%   ndeg    Degree of Taylor polynomial
%
% Author    Clas Veib�ck

  c = 343;
  [n, N] = size(doa);
  [M, m] = size(Y);
    
  if nargout > 1; SE = true; else; SE = false; end
    
  nl = 0:ndeg;
  Tinit = [1 repmat(1./factorial(nl(2:end)), 1, n)];
  P = nan(size(doa));
  S = zeros(size(Y,1), ndeg * n + 1, size(doa,2));
  for i=1:size(doa,2)
    
    % Full derivatives
    v = [cos(doa(:,i)'); sin(doa(:,i)');0*doa(:,i)'];
    dt = -pos'*v/c;
    T = Tinit.*[ones(m,1) kron(-dt,ones(1,ndeg)).^kron(ones(1,n),nl(2:end))];
    
    Shat = T\Y';
    
    if SE
      eps = Y'-T*Shat;
      P(i) = -sum(eps(:).^2);
    end
    
    S(:,:,i) = Shat';
  end
end
